//
//  SecondViewController.swift
//  Do You Feel Lucky, Punk? Well Do Ya?
//
//  Created by Ravipati,Havya on 2/26/19.
//  Copyright © 2019 Ravipati,Havya. All rights reserved.
//

import UIKit

class GuesserHistoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource  {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Guesser.shared.numGuesses()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GuesserHistory")!
        cell.textLabel?.text = "Correct Answer: " + String(Guesser.shared[indexPath.row].correctAnswer)
        cell.detailTextLabel?.text = "# Attempts:" + String(Guesser.shared[indexPath.row].numAttemptsRequired)
        return cell    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    @IBOutlet weak var historyTV: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        historyTV.dataSource = self
        historyTV.delegate = self
        // Do any additional setup after loading the view, typically from a nib.
    }
    override func viewWillAppear(_ animated: Bool) {
        historyTV.reloadData()
    }
}

