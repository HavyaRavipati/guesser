//
//  Guesser.swift
//  Do You Feel Lucky, Punk? Well Do Ya?
//
//  Created by Ravipati,Havya on 2/27/19.
//  Copyright © 2019 Ravipati,Havya. All rights reserved.
//

import Foundation

enum Result:String{case tooLow = "Too Low", tooHigh = "Too High", correct = "Correct"}

class Guesser{
    private var correctAnswer:Int
    private var _numAttempts:Int
    private var guesses:[Guess]
    public struct Guess{
        var correctAnswer:Int
        var numAttemptsRequired:Int
    }
    var numAttempts:Int{
        return _numAttempts
    }
    private init(){
        self.correctAnswer = 0
        self._numAttempts = 0
        self.guesses = []
    }
    static var shared = Guesser()
    func createNewProblem(){
        correctAnswer = Int.random(in: 1...10)
        _numAttempts = 0
        
    }
    func amIRight(guess: Int) -> Result{
        _numAttempts = _numAttempts + 1
        if guess < correctAnswer{
            return Result.tooLow
        }
        else if guess > correctAnswer{
            return Result.tooHigh
        }
        else{
            guesses.append(Guess(correctAnswer: correctAnswer, numAttemptsRequired: _numAttempts))
            return Result.correct
        }
    }
    func guess(index:Int) -> Guess{
        return guesses[index]
    }
    func numGuesses() -> Int{
        return guesses.count
    }
    func clearStatistics(){
        guesses = []
    }
    func minimumNumAttempts() -> Int{
        if guesses.count != 0{
        var minimum:Int = guesses[0].numAttemptsRequired
        for i in guesses{
            if minimum > i.numAttemptsRequired{
                minimum = i.numAttemptsRequired
            }
        }
            return minimum}
        else{
            return 0
        }
    }
    func maximumNumAttempts() -> Int{
        if guesses.count != 0{
            
        
        var maximum:Int = guesses[0].numAttemptsRequired
        for i in guesses{
            if maximum < i.numAttemptsRequired{
                maximum = i.numAttemptsRequired
            }
        }
            return maximum}
        else{
            return 0
        }
    }
    subscript(guess:Int) -> Guess{
        return guesses[guess]
    }

}

