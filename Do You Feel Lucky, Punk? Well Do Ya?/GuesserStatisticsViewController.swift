//
//  GuesserStatistics.swift
//  Do You Feel Lucky, Punk? Well Do Ya?
//
//  Created by Ravipati,Havya on 2/27/19.
//  Copyright © 2019 Ravipati,Havya. All rights reserved.
//

import UIKit

class GuesserStatisticsViewController: UIViewController {

    @IBOutlet weak var minimumLBL: UILabel!
    @IBOutlet weak var maximumLBL: UILabel!
    @IBOutlet weak var meanLBL: UILabel!
    @IBOutlet weak var stddevLBL: UILabel!
    @IBAction func clearBTN(_ sender: Any) {
        minimumLBL.text = "0"
        maximumLBL.text = "0"
        meanLBL.text = "0"
        stddevLBL.text = "0"
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        labels()
    }
    
    func labels(){
        minimumLBL.text = String(Guesser.shared.minimumNumAttempts())
        maximumLBL.text = String(Guesser.shared.maximumNumAttempts())
        var incr = 0
        for i in 0..<Guesser.shared.numGuesses(){
            incr += Guesser.shared.guess(index: i).numAttemptsRequired
        }
        let mean = Double(incr)/Double(Guesser.shared.numGuesses())
        meanLBL.text = "\(mean)"
        var sum = 0.0
        for i in 0..<Guesser.shared.numGuesses(){
            sum += pow(Double(Guesser.shared.guess(index: i).numAttemptsRequired) - mean, 2)
        }
        stddevLBL.text = "\(sum/Double(Guesser.shared.numGuesses()))"
    }

    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
