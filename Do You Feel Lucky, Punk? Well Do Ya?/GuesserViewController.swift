//
//  FirstViewController.swift
//  Do You Feel Lucky, Punk? Well Do Ya?
//
//  Created by Ravipati,Havya on 2/26/19.
//  Copyright © 2019 Ravipati,Havya. All rights reserved.
//

import UIKit

class GuesserViewController: UIViewController {

    @IBOutlet weak var userguessTF: UITextField!
    @IBOutlet weak var resultLBL: UILabel!
    
    @IBAction func amirightBTN(_ sender: Any) {
        if let value = Int(userguessTF.text!){
           
            if value >= 1 && value <= 10
            {
                 let output = Guesser.shared.amIRight(guess : value)
            if output == Result.correct{
                resultLBL.text = output.rawValue
                displayMessage()
                Guesser.shared.createNewProblem()
            }
            else{
                resultLBL.text = output.rawValue
                }
            }
            else{
                error()
            }
        }
        else{
            error()
        }
    }
    
    @IBAction func CreateNewProblemBTN(_ sender: Any) {
        resultLBL.text = ""
        userguessTF.text = ""
        Guesser.shared.createNewProblem()
    }
    
    func displayMessage(){
        let alert = UIAlertController(title: "Wonderful", message: "You got it in \(Guesser.shared.numAttempts) tries", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok!", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func error(){
        let alert = UIAlertController(title: "Oops", message: "You have entered an value that is not valid. Enter a value ranging from 1 to 10.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok!", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        Guesser.shared.createNewProblem()
        // Do any additional setup after loading the view, typically from a nib.
    }
}

